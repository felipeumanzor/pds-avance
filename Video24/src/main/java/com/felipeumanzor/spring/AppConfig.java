package com.felipeumanzor.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.felipeumanzor.beans.Barcelona;
import com.felipeumanzor.beans.Camiseta;
import com.felipeumanzor.beans.Jugador;
import com.felipeumanzor.beans.Juventus;
import com.felipeumanzor.beans.Marca;

@Configuration
public class AppConfig {
	
	@Bean
	public Jugador jugador1(){
		return new Jugador();
	}
	
	@Bean
	public Barcelona barcelona(){
		return new Barcelona();
	}
	
	@Bean
	public Juventus juventus(){
		return new Juventus();
	}
	
	@Bean
	public Camiseta camiseta(){
		return new Camiseta();
	}
	
	@Bean
	public Marca marca(){
		return new Marca();
	}
}
