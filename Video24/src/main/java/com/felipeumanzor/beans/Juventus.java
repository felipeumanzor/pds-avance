package com.felipeumanzor.beans;

import org.springframework.stereotype.Component;

import com.felipeumanzor.interfaces.IEquipo;

@Component
public class Juventus implements IEquipo{

	public String mostrar() {
		return "Juventus";
	}
}
