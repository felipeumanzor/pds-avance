package com.felipeumanzor.beans;

import org.springframework.stereotype.Component;

import com.felipeumanzor.interfaces.IEquipo;

@Component
public class Barcelona implements IEquipo{

	public String mostrar() {
		return "Barcelona FC";
	}

}
