package com.felipeumanzor.spring;

import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.felipeumanzor.beans.Barcelona;
import com.felipeumanzor.beans.Ciudad;
import com.felipeumanzor.beans.Jugador;
import com.felipeumanzor.beans.Persona;
import com.felipeumanzor.interfaces.IEquipo;

public class App {

	public static void main(String[] args) {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/felipeumanzor/xml/beans.xml");

		//Jugador jug = (Jugador) appContext.getBean("messi");
		IEquipo bar = (IEquipo) appContext.getBean("barcelona");
		IEquipo juv = (IEquipo) appContext.getBean("juventus");
		
		System.out.println(bar.mostrar());
		System.out.println(juv.mostrar());
		//System.out.println(jug.getNombre()+ "-" + jug.getEquipo().mostrar());
		

		((ConfigurableApplicationContext)appContext).close();
	}

}
