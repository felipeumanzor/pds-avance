package com.felipeumanzor.service;

import com.felipeumanzor.beans.Jugador;

public interface ServiceJugador {
	public void registrar(Jugador jug) throws Exception;
}
