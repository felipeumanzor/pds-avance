package com.felipeumanzor.springbd;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.felipeumanzor.beans.Equipo;
import com.felipeumanzor.beans.Jugador;
import com.felipeumanzor.beans.Marca;
import com.felipeumanzor.service.ServiceJugador;
import com.felipeumanzor.service.ServiceMarca;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        
        ApplicationContext appContext = new ClassPathXmlApplicationContext("com/felipeumanzor/xml/beans.xml");
        
        ServiceJugador sj = (ServiceJugador) appContext.getBean("serviceJugadorImpl");
        Jugador jug1 = (Jugador) appContext.getBean("jugador1");
        try {
			sj.registrar(jug1);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
        
        
    }
}
