package com.felipeumanzor.dao;

import com.felipeumanzor.beans.Jugador;

public interface DAOJugador {
	public void registrar(Jugador jugador) throws Exception;
}
