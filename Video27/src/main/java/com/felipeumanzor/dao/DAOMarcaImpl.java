package com.felipeumanzor.dao;

import java.sql.Connection;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.felipeumanzor.beans.Marca;
import java.sql.PreparedStatement;

@Repository
public class DAOMarcaImpl implements DAOMarca{

	@Autowired
	private DataSource dataSource;
	public void registrar(Marca marca) throws Exception {
		String sql = "INSERT INTO marca(id,nombre) values(?,?)";
		Connection con = null;
		
		try {
			con = dataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, marca.getId());
			ps.setString(2, marca.getNombre());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if(con != null){
				con.close();
			}
		}
		
	}

}
