package com.felipeumanzor.dao;

import com.felipeumanzor.beans.Marca;

public interface DAOMarca {
	public void registrar(Marca marca) throws Exception;
}
