package com.felipeumanzor.service;

import com.felipeumanzor.beans.Marca;

public interface ServiceMarca {
	public void registrar(Marca marca) throws Exception;
}
