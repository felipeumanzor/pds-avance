package com.felipeumanzor.spring;

//import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.felipeumanzor.beans.AppConfig;
import com.felipeumanzor.beans.AppConfig2;
import com.felipeumanzor.beans.Mundo;

public class App {

	public static void main(String[] args) {
        AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext();
        appContext.register(AppConfig.class);
        appContext.register(AppConfig2.class);
        appContext.refresh();
        Mundo m = (Mundo) appContext.getBean("marte");
        
        System.out.println(m.getSaludo());
        
        ((ConfigurableApplicationContext)appContext).close();
	}

}
